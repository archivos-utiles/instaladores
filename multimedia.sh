#!/bin/sh
# Script para la instalación de aplicaciones gráficas

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones gráficas'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para multimedia...';

echo '';

echo 'Iniciando la instalación de moc';
aptitude install moc;

echo 'Iniciando la instalación de WIinFF';
aptitude install winff;

echo 'Iniciando la instalación de Flash';
aptitude install flashplugin-nonfree;

echo 'Iniciando la instalación de AlarmClock';
aptitude install alarm-clock;

echo 'Iniciando la instalación de Amarok';
aptitude install amarok;

echo 'Iniciando la instalación de VLC';
aptitude install vlc;

echo 'Iniciando la instalación de Brasero';
aptitude install brasero;

echo 'Iniciando la instalación de Cheese';
aptitude install cheese;

echo 'Iniciando la instalación de OpenShot';
aptitude install openshot;

echo 'Iniciando la instalación de audacity';
aptitude install audacity;

echo 'Iniciando la instalación de turpial';
aptitude install turpial;

echo 'Iniciando la instalación de miro';
aptitude install miro;

echo 'Iniciando la instalación de soundconverter';
aptitude install soundconverter;

echo 'Iniciando la instalación de RecordMyDesktop';
aptitude install gtk-recordmydesktop recordmydesktop;
"
