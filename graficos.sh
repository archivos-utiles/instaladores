#!/bin/sh
# Script para la instalación de aplicaciones gráficas

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones gráficas'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para multimedia...';

echo '';

echo 'Iniciando la instalación de Vym';
aptitude install vym;

echo 'Iniciando la instalación de Shotwell';
aptitude install shotwell;

echo 'Iniciando la instalación de PDFEdit';
aptitude install pdfedit;

echo 'Iniciando la instalación de Scribus';
aptitude install scribus;

echo 'Iniciando la instalación de Inkscape';
aptitude install inkscape;

echo 'Iniciando la instalación de Blender';
aptitude install blender;

echo 'Iniciando la instalación de Simple Scan';
aptitude install simple-scan;

echo 'Iniciando la instalación de Darktable';
aptitude install darktable;

echo 'Iniciando la instalación de Dia';
aptitude install dia;

echo 'Iniciando la instalación de Gimp';
aptitude install gimp;

echo 'Iniciando la instalación de Xdot';
aptitude install xdot;
"
