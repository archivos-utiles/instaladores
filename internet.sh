#!/bin/sh
# Script para la instalación de aplicaciones de internet

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones de internet'
exec su -c "
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para internet';

echo '';

echo 'iniciando la instalación de w3m';
aptitude install w3m;

echo 'iniciando la instalación de multiget';
aptitude install multiget;

echo 'Iniciando la instalación de Emesene';
aptitude install emesene;

echo 'Iniciando la instalación de aMSN';
aptitude install amsn;

echo 'Iniciando la instalación de aMSN';
aptitude install amsn;

echo 'Iniciando la instalación de Skype';
aptitude install skype;

echo 'Iniciando la instalación de TeamSpeak';
aptitude install teamspeak-client;

echo 'Iniciando la instalación de TeamViewer';
aptitude install teamviewer7;

echo 'Iniciando la instalación de Transmission';
aptitude install transmission;

echo 'Iniciando la instalación de Ekiga';
aptitude install ekiga;

echo 'Iniciando la instalación de Quassel';
aptitude install quassel;
"
