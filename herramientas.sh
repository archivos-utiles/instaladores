# Script para la instalación de aplicaciones de herramientas

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones de herramientas'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para herramientas...';

echo '';

echo 'Iniciando la instalación de nmap';
aptitude install nmap nmapsi4 nmapfe;

echo 'Iniciando la instalación de UnetBooting';
aptitude install unetbootin;

echo 'Iniciando la instalación de Liferea';
aptitude install liferea;

echo 'Iniciando la instalación de TestDisk';
aptitude install testdisk;

echo 'Iniciando la instalación de VirtualBox';
aptitude install virtualbox;

echo 'Iniciando la instalación de Conduit';
aptitude install conduit;

echo 'Iniciando la instalación de GNote';
aptitude install gnote;

echo 'Iniciando la instalación de GnomeSystemLog';
aptitude install gnome-system-log;

echo 'Iniciando la instalación de Nautilus Action';
aptitude install nautilus-actions;

echo 'Iniciando la instalación de Nautilus compare';
aptitude install nautilus-compare;

echo 'Iniciando la instalación de Nautilus Image Converter';
aptitude install nautilus-image-converter;

echo 'Iniciando la instalación de Nautilus Share';
aptitude install nautilus-share;

echo 'Iniciando la instalación de Resolvconf';
aptitude install wvdial resolvconf;
"
