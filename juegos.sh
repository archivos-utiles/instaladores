#!/bin/sh
# Script para la instalación de aplicaciones gráficas

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones gráficas'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para multimedia...';

echo '';

echo 'Iniciando la instalación de frozen-burble';
aptitude install frozen-burble;

echo 'Iniciando la instalación de childsplay';
aptitude install childsplay;

echo 'Iniciando la instalación de supertux';
aptitude install supertux;

echo 'Iniciando la instalación de GBrainy';
aptitude install gbrainy;

echo 'Iniciando la instalación de GCompris';
aptitude install gcompris;

echo 'Iniciando la instalación de Open Arena';
aptitude install openarena;

echo 'Iniciando la instalación de Alien Arena';
aptitude install alien-arena;

echo 'Iniciando la instalación de Gnome Games';
aptitude install gnome-games;
"
