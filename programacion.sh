#!/bin/sh
# Script para la instalación de aplicaciones gráficas

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones gráficas'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para programar...';

echo 'Iniciando la instalación de Geany';
aptitude install geany;

echo 'Iniciando la instalación de Gambas3';
aptitude install gambas3;

echo 'Iniciando la instalación de Glade';
aptitude install glade;

echo 'Iniciando la instalación de Eclipse';
aptitude install eclipse;

echo 'Iniciando la instalación de Ninja IDE';
aptitude install ninja-ide;

echo 'Iniciando la instalación de SSH';
aptitude install ssh openssh-client openssh-server;

echo 'Iniciando la instalación de Vim';
aptitude install vim;

echo 'Iniciando la instalación de Build Essentials';
aptitude install build-essential;

echo 'Iniciando la instalación de Django';
aptitude install python-django;

echo 'Iniciando la instalación de Pip';
aptitude install python-pip;

echo 'Iniciando la instalación de Canaima Desarrollador';
aptitude install canaima-desarrollador;

echo 'Iniciando la instalación de git';
aptitude install git;

echo 'Iniciando la instalación de gource';
aptitude install gource;
"
