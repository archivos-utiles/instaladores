#!/bin/sh
# Script para la instalación de aplicaciones de programación automáticamente

echo 'Es necesaria su contraseña root para iniciar las instalaciones de aplicaciones para programar'
exec su -c "
aptitude update;
echo 'Iniciando la instalación de aplicaciones necesarias y/o importantes para programar...';

echo 'Iniciando la instalación de Gource, Gambas3, Git, Canaima Desarrollador, Pip, Django, Build Essentials, Vim, SSH, Geany, Glade, Eclipse y Ninja IDE';
aptitude install geany gambas3 glade eclipse ssh openssh-client openssh-server vim build-essential python-django python-pip canaima-desarrollador git gource ninja-ide;
"
